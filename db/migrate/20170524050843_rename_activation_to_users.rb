class RenameActivationToUsers < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :activation, :activated
  end
end
